/**
 * Created by rameshpaul on 6/4/16.
 */
jQuery(document).ready(function(){

});

var sermonPage = {};

sermonPage.init = function(){

}
sermonPage.selectedSermon = '';

sermonPage.loadSermon = function(item, url, type, flag){
    jQuery("#post-container-div").hide();
    jQuery("#sermon-preview-div").show();

    var elm = jQuery(item);
    var json = jQuery(elm[0]).attr("data-info");
    var code = decodeURIComponent(json);
    var data = JSON.parse(code);
    sermonPage.selectedSermon = data;

    if(type == 'audio'){
        sermonPage.loadAudio(item,url,flag,data);
        sermonPage.showPreviewContent(item,data);
    } else if(type == 'video'){
        sermonPage.loadVideo(item,url,flag,data);
        sermonPage.showPreviewContent(item,data);
    }

    return false;
};

sermonPage.showPreviewContent = function(item,data){
    /*var elm = jQuery(item);
    var json = jQuery(elm[0]).attr("data-info");
    var code = decodeURIComponent(json);
    var data = JSON.parse(code);
    console.log(elm,jQuery(elm[0]),json,code,data);*/
    jQuery("#sermon-preview-div .sermon-info .title").html(data.title);
    jQuery("#sermon-preview-div .sermon-info .date").html(data.sermondate);
    jQuery("#sermon-preview-div .sermon-info .preacher").html(data.preacherName);
    jQuery("#sermon-preview-div .sermon-info .bible-text").html(data.sermonBiblePassage);
    jQuery("#sermon-preview-div .sermon-info .series").html(data.seriesName);
}

sermonPage.loadAudio = function(item, url, flag,data){
    jQuery("#sermon-preview-div .preview-audio").removeClass('preview-ser');
    jQuery("#sermon-preview-div .preview-audio").show();
    jQuery("#sermon-preview-div .sermon-options-block .audio").addClass('sermon-preview-tag-active');
    jQuery("#jp_container_audio").html('<audio src="'+url+'" preload="auto" />');
    setTimeout(function(){
        audiojs.events.ready(function() {
            var as = audiojs.createAll();
        });
    },5000);
}

sermonPage.loadVideo = function(item, url, flag, data){
    jQuery("#sermon-preview-div .preview-video").removeClass('preview-ser');
    jQuery("#sermon-preview-div .preview-video").show();//sermon-preview-tag-active
    jQuery("#sermon-preview-div .sermon-options-block .video").addClass('sermon-preview-tag-active');
    /*console.log(item,url,flag);
    var elm = jQuery(item);
    var json = jQuery(elm[0]).attr("data-info");
    var code = decodeURIComponent(json);
    var data = JSON.parse(code);*/
    jQuery("#sermon-preview-div .preview-video").html(data.sermonVideo);

}

sermonPage.switchSermon = function(type){
    var data = sermonPage.selectedSermon;

    if(type == 'audio'){
        sermonPage.loadAudio(item,url,flag,data);
        sermonPage.showPreviewContent(item,data);
    } else if(type == 'video'){
        sermonPage.loadVideo(item,url,flag,data);
        sermonPage.showPreviewContent(item,data);
    }

    return false;
};

sermonPage.clearAudio = function(item, url, flag, data){
    jQuery("#jp_container_audio").html('');
}

sermonPage.clearVideo = function(item, url, flag, data){
    jQuery("#jp_container_audio").html('');
    jQuery("#sermon-preview-div .preview-video").html('');
}

sermonPage.closePreview = function(){
    jQuery("#post-container-div").show();
    jQuery("#sermon-preview-div").hide();

    jQuery("#jp_container_audio").html('');
    jQuery("#sermon-preview-div .preview-video").html('');
    jQuery("#sermon-preview-div .sermon-options-block span").removeClass('sermon-preview-tag-active');
}

window.loadSermon = sermonPage.loadSermon;
window.closePreview = sermonPage.closePreview;
window.switchSermon = sermonPage.switchSermon;

