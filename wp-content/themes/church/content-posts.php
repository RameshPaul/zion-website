<?php 
/**
 * 
 * @package Causes 
 */
?>
<div class="section-inner-page">
	<div class="container">
		<div class="column-container">
			<div class="column-9-12 left">
				<div class="gutter">
					<div class="page-container" id="post-container-div">
						<?php
                            while (have_posts()) : the_post();
                                if($post->post_type == 'wpfc_sermon'){
                            ?>
                                    <!--<div class="sermonBlock">
                                        <div class="embedVideoBlock"></div>
                                        <div class="titleOptionsGroup">
                                            <a href="/sermons-en#!sid=1351.1" class="sermonTitle" onclick="loadSermonPage('1351.1', false, false); return false;">Riches and patience</a>
                                            <div class="infoLine">
                                                <a class="sermonInfoValue" href="/sermons-en">English</a>
                                                <i class="pipe">|</i> by:
                                                <a class="sermonInfoValue" href="javascript:filterBySpeaker('CHUCK_BAGGETT', true);">Chuck Baggett</a>
                                                <i class="pipe">|</i> Series:
                                                <a class="sermonInfoValue" href="javascript:filterBySeries('The%20Epistle%20of%20James', true);">The Epistle of James</a>
                                                <span class="sermonInfoValue">&nbsp;#10</span>
                                                <i class="pipe">|</i> Date:
                                                <span class="sermonInfoValue">August 7, 2015</span>
                                                <i class="pipe">|</i>
                                                <a class="sermonInfoValue" href="javascript:filterByScripture('JAS', 5, true);">James 5:1-11</a>
                                            </div>
                                            <a href="/sermons-en#!sid=1351.1" class="sermonTitleTranslation" onclick="loadSermonPage('1351.1', false, false); return false;" style="display: none;"></a>
                                            <div class="infoLineTranslation" style="display: none;"></div>
                                            <div class="optionsBlock">
                                                <a class="streamAudio" href="/sermons-en#!sid=1351.1" onclick="loadSermonPage('1351.1', false, true); return false;">Audio</a>
                                                <a class="streamVideo" href="/sermons-en#!sid=1351.1" onclick="loadSermonPage('1351.1', false, false); return false;">Video</a>
                                                <span class="downloadMedia">Download:
                                                    <a class="downloadAudio" download="" href="http://s3.amazonaws.com/churchplantmedia-cms/heartcry_missionary_society_radford_va/epistle-of-james-10_1.mp3?f=1" onclick="trackAudioDownload('en', '/sermons-en#!sid=1351.1')">Audio</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>-->
                            <?php the_excerpt(); ?>

                            <?php } else { ?>
                                <article class="article-blog">
                                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                                        <div class="article-image">
                                            <div class="img-container">
                                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail($post->ID, 'featured'); ?></a>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <h2><a href="<?php the_permalink() ?>"><?php if(get_the_title($post->ID)) { the_title(); } else { the_time( get_option( 'date_format' ) ); } ?></a></h2>
                                        <p class="meta"><?php the_time( get_option( 'date_format' ) ); ?> / <?php the_author(); ?> / <?php the_category(', '); ?></p>
                                        <?php the_excerpt(); ?>
                                        <p class="more"><a class="button" href="<?php the_permalink() ?>"><?php _e( 'Read More', 'causes' ); ?></a></p>
                                    </div>
                                </article>
						<?php  }
                            endwhile; ?>


						<span class="left button-gray"><?php next_posts_link(__('Next Posts', 'causes')) ?></span>
						<span class="right button-gray"><?php previous_posts_link(__('Previous posts', 'causes')) ?></span>
					</div>
                    <div class="sermon-preview preview-ser" id="sermon-preview-div">
                        <div id="sermon-preview-close" class="back-btn">
                            <button class="btn btn-primary float-right" onclick="closePreview(this)">
                                <i class="fa fa-caret-left"></i> Back
                            </button>
                        </div>
                        <div class="sermon-info">
                            <h3 class="title"></h3>
                            <p> <b>By :</b> <span class="preacher"></span></p>
                            <p> <b>Date :</b> <span class="date"></span></p>
                            <p> <b>Bible Text :</b> <span class="bible-text"></span></p>
                            <p> <b>Series :</b> <span class="series"></span></p>
                        </div>
                        <!--<div class="sermon-options-block">
                            <span class="audio"> <i class="fa fa-headphones"></i> Audio </span>
                            <span class="video"> <i class="fa fa-youtube-play"></i> Video </span>
                            <span class="notes"> <i class="fa fa-sticky-note"></i> Notes </span>
                            <span class="share"> <i class="fa fa-share"></i> Share </span>
                        </div>-->
                        <div class="preview-audio preview-ser" style="display: none;">
                            <div id="jp_container_audio" class="jp-audio" role="application" aria-label="media player">
                            </div>
                        </div>
                        <div class="preview-video preview-ser"  style="display: none;">

                        </div>
                        <div class="preview-notes preview-ser"  style="display: none;">

                        </div>
                    </div>
				</div>
			</div>
			<div class="column-3-12 right">
				<div class="gutter">
					<?php  get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>
</div>