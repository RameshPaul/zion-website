<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package Causes
 */
?>
<div class="clearfooter"></div>
</div>
<footer id="footer" class="footer">

    <div class="copyright-block">
        <div class="container">
            <div class="column-container">
                <div class="column-12-12">
                    <div class="gutter">
                        <p class="text-center"><?php echo sprintf( __('Copyright &copy; 2016 %s All Rights Reserved.', 'causes'), get_bloginfo('name') );?></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>		
</body>
</html>