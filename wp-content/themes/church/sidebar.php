<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Causes
 */
?>
<div class="sidebar-container">
	<?php if ( is_active_sidebar('blog-sidebar') ) : ?>
		<?php dynamic_sidebar('blog-sidebar'); ?>
	<?php else : ?>	

	<?php endif; ?>
</div>