/*
Theme Name: Church Theme
Author: Ramesh Paul
Theme URI: http://chrameshbabu.blogspot.in/
Author URI: http://chrameshbabu.blogspot.in/
Description: Church Theme is a simple and rich theme for churches and missionary organizations.
Version: 1.0.0
Tags: church, church theme, missionary, missionary organization, christian organization
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Text Domain: church
*/

== Copyright ==
Church WordPress Theme, Copyright (C) 2015 Ramesh Paul
Church WordPress Theme is licensed under the GPL 3.

Church is built with the following resources:

Plugin kirki - https://wordpress.org/plugins/kirki/
License: GPLv2 or later
Copyright: Aristeides Stathopoulos, http://aristeides.com

Owl Carousel - https://github.com/OwlFonk/OwlCarousel
License: MIT 
Copyright: owlgraphic, http://owlgraphic.com/owlcarousel/

Open Sans - http://www.fontsquirrel.com/fonts/open-sans
License: Apache License v2.00
Copyright: Ascender Fonts, http://www.ascenderfonts.com

Roboto - http://www.fontsquirrel.com/fonts/roboto
License: Apache License v2.00
Copyright: Christian Robertson, http://christianrobertson.com/

Ubuntu - http://www.fontsquirrel.com/fonts/ubuntu
License: UBUNTU FONT LICENCE v1.00
Copyright: Dalton Maag Ltd, http://www.daltonmaag.com


Images on the screenshot:  http://pixabay.com/en/children-boy-girl-young-innocent-82272/
License: CC0 Public Domain / FAQ
Copyright: drkiranhania, http://pixabay.com/

== Installation ==

1. Upload the `Church` folder to the `/wp-content/themes/` directory
Activation and Use
1. Activate the Theme through the 'Themes' menu in WordPress
2. See Appearance -> Theme Options to change theme options
